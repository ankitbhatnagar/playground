package main

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/pkg/clouddns/gcp"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/common"
	"google.golang.org/api/dns/v1"
)

func setupDNS(testID, expectedDNSName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	// setup managed zone
	devCredentialsFile := os.Getenv("DEV_REALM_CREDENTIALS_FILE")
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", devCredentialsFile); err != nil {
		return err
	}
	provider, err := gcp.NewDNSProvider(ctx, common.DevRealmProjectName)
	if err != nil {
		return err
	}

	expectedName := strings.ReplaceAll(expectedDNSName, ".", "-")
	expectedDNSName = fmt.Sprintf("%s.", expectedDNSName) // trailing dot is important!
	expectedManagedZone := dns.ManagedZone{
		Name:        expectedName,
		DnsName:     expectedDNSName,
		Description: fmt.Sprintf("managed zone for GOP integration test: %s", testID),
	}

	// check if the expected zone name already exists
	zones, err := provider.ListManagedZones(ctx, common.DevRealmProjectName)
	fmt.Printf("%+v", zones)
	if err != nil {
		return err
	}

	if _, ok := zones[expectedName]; ok {
		fmt.Println("managed zone for GOP integration test already exists")
		return nil // nothing to do
	}

	// create managed zone
	createdManagedZone, err := provider.CreateManagedZone(common.DevRealmProjectName, &expectedManagedZone)
	if err != nil {
		return err
	}

	// setup NS delegate
	developmentCredentialsFile := os.Getenv("DEVELOPMENT_CREDENTIALS_FILE")
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", developmentCredentialsFile); err != nil {
		return err
	}

	provider, err = gcp.NewDNSProvider(ctx, common.DevelopmentProjectName)
	if err != nil {
		return err
	}

	// https://cloud.google.com/dns/docs/records#api
	expectedChange := dns.Change{
		Additions: []*dns.ResourceRecordSet{
			{
				Type:    gcp.RecordTypeNS,
				Name:    expectedDNSName,
				Ttl:     300,
				Rrdatas: createdManagedZone.NameServers,
			},
		},
	}

	_, err = provider.CreateRecords(common.DevelopmentProjectName, common.RootManagedZoneName, &expectedChange)
	if err != nil {
		return err
	}
	return nil
}

func destroyDNS(testID, expectedDNSName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	// setup managed zone
	devCredentialsFile := os.Getenv("DEV_REALM_CREDENTIALS_FILE")
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", devCredentialsFile); err != nil {
		return err
	}
	provider, err := gcp.NewDNSProvider(ctx, common.DevRealmProjectName)
	if err != nil {
		return err
	}

	expectedName := strings.ReplaceAll(expectedDNSName, ".", "-")
	expectedDNSName = fmt.Sprintf("%s.", expectedDNSName) // trailing dot is important!
	expectedManagedZone := dns.ManagedZone{
		Name:        expectedName,
		DnsName:     expectedDNSName,
		Description: fmt.Sprintf("managed zone for GOP integration test: %s", testID),
	}

	// first check if the expected zone name exists
	zones, err := provider.ListManagedZones(ctx, common.DevRealmProjectName)
	if err != nil {
		return err
	}

	if _, ok := zones[expectedName]; ok {
		// delete managed zone
		if err := provider.DeleteManagedZone(common.DevRealmProjectName, &expectedManagedZone); err != nil {
			return err
		}
	} else {
		fmt.Println("managed zone for GOP integration test does not exist")
	}

	// remove NS delegation from root zone
	developmentCredentialsFile := os.Getenv("DEVELOPMENT_CREDENTIALS_FILE")
	if err := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", developmentCredentialsFile); err != nil {
		return err
	}

	provider, err = gcp.NewDNSProvider(ctx, common.DevelopmentProjectName)
	if err != nil {
		return err
	}

	rrsets, err := provider.ListResourceRecordSets(ctx, common.DevelopmentProjectName, common.RootManagedZoneName)
	if err != nil {
		return err
	}

	var toDelete *dns.ResourceRecordSet
	for _, rrset := range rrsets {
		if rrset.Name == expectedDNSName {
			toDelete = rrset
		}
	}
	if toDelete == nil {
		return nil // nothing to do
	}
	// https://cloud.google.com/dns/docs/records#remove_a_record
	expectedChange := dns.Change{
		Deletions: []*dns.ResourceRecordSet{toDelete},
	}
	// deletion change set is also "created" theoretically
	_, err = provider.CreateRecords(common.DevelopmentProjectName, common.RootManagedZoneName, &expectedChange)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	// setup DNS
	// fmt.Println("Setting up DNS records")
	// if err := setupDNS("", "gitlab-observability-demos.opstracegcp.com"); err != nil {
	// 	panic(err)
	// }
	// if err := setupDNS("", fmt.Sprintf(common.PlatformDomainNameTmpl, testIdentifier)); err != nil {
	// 	panic(err)
	// }
	// fmt.Println("Sleeping for 30 seconds for records to converge...")
	// time.Sleep(30 * time.Second)
	testIdentifiers := []string{
		"450b77fab",
		"607b21fa",
		"8ba2b20d",
	}
	for _, testIdentifier := range testIdentifiers {
		fmt.Printf("Destroying DNS records: %s\n", testIdentifier)
		// destroy DNS setup specific to this test run
		if err := destroyDNS(testIdentifier, fmt.Sprintf(common.GitlabDomainNameTmpl, testIdentifier)); err != nil {
			fmt.Println(err)
		}
		if err := destroyDNS(testIdentifier, fmt.Sprintf(common.PlatformDomainNameTmpl, testIdentifier)); err != nil {
			fmt.Println(err)
		}
	}

}
